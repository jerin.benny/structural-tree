#ifndef STRUCTREE_H
#define STRUCTREE_H
#include <string>
#include <iostream>
#include <complex>
#include <stdlib.h>
#include <vector>

#define PI 3.1415926

using namespace std;
class structree{
    public:
    structree(){};
    virtual ~structree(){};
    vector<float>ifft(vector<complex<float> > &Y);
    vector<complex<float> >fft(vector<complex<float> > &Y);
    vector<complex<float> > ft(vector<complex<float> > &Y);
    vector< vector< complex<float> > > prl(vector<vector<complex<float> > > &templ,vector<vector<complex<float> > > &tempr);
    complex<float> det(vector<complex<float> > &Y);
    vector< vector< complex<float> > > srs(vector<vector<complex<float> > > &templ,vector<vector<complex<float> > > &tempr);
    vector <vector <complex<float> > > admit(float r, int gen);
    vector<vector<float> > initstruc();

    void set_r0(float _A0){r0=sqrt(_A0/PI);}
    void set_vr0(float _A1){vr0=sqrt(_A1/PI);}
    void set_T(float _T){T=_T;}
    void set_tstep(float _tstep){t_step=_tstep;}
    void set_maxh(){maxharmonic=T/t_step/2;}
    vector<vector<float> > get_timeadmit(){return timeadmit;}
    
    
    protected:
    
    private:
    
	float alpha=0.91;
	float beta=0.58;
	float rmin=0.005;
	float r0;
	float vr0;
	float rho=1.055;
	float mu=0.49;
	float T;
	float comp;
	float t_step;
	int maxharmonic;
	vector<complex<float> > Y1;
	vector<complex<float> > Y2;
	vector<complex<float> > Y3;
	vector<complex<float> > Y4;
	vector<vector<float> > timeadmit;
	vector<complex<float> >admat;
	vector< vector<float> > rmtrx;
	vector< vector< vector <vector <complex<float> > > > > Ymtrx;
	void writeHist(const char* desti_File, vector<complex<float> > data)const;
};
#endif
