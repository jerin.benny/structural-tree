#include "structree.h"

using namespace std;

vector<float>structree::ifft(vector<complex<float> > &Y){
	float N=Y.size();
	vector<complex<float> > unmodres=fft(Y);
	vector<float> inv;
	for(int i=0;i<N;i++){
	inv.push_back(real(unmodres[i])/N);
	}
	return inv;
}

vector<complex<float> >structree::fft(vector<complex<float> > &Y){
	vector<complex<float> > out;
	if(Y.size()%2==0){
		vector<complex<float> > odd;
		vector<complex<float> > even;
		vector<complex<float> > out1;
		vector<complex<float> > out2;
		for(int i=0;i<(Y.size()/2);i++){
			even.push_back(Y[2*i]);
			odd.push_back(Y[2*i+1]);
		}
		out1=fft(even);
		out2=fft(odd);
		for(int i=0;i<out1.size();i++){
			complex<float>storage(0,0);
			complex<float> argument2(0,2*PI*i/(2*out1.size()));
			storage=out1[i]+out2[i]*exp(argument2);
			out.push_back(storage);
		}
		for(int i=out1.size();i<2*out1.size();i++){
			complex<float>storage(0,0);
			complex<float> argument2(0,2*PI*(i-out1.size())/(2*out1.size()));
			storage=out1[(i-out1.size())]-out2[(i-out1.size())]*exp(argument2);
			out.push_back(storage);
		}
	}
	else{	
		out=ft(Y);
	}
	return out;
}

vector<complex<float> >structree::ft(vector<complex<float> > &Y){
	vector<complex<float> > out;
	int sigN=Y.size();
	for(int i=0;i<sigN;i++){
		complex<float>timeval=0;
		for(int j=0;j<sigN;j++){
			timeval+=Y[j]*exp(complex<float>(0,2*PI*i*j/sigN));
		}
		out.push_back(timeval);
	}
	return out;
}


vector< vector< complex<float> > > structree::prl(vector<vector<complex<float> > > &templ,vector<vector<complex<float> > > &tempr){
    vector< vector< complex< float > > > temptube;
    for (int r=0;r<maxharmonic;r++){
        vector<complex<float> > tempharm;
        for (int index=0;index<4;index++){
        	tempharm.push_back(templ[r][index]+tempr[r][index]);
        }
        temptube.push_back(tempharm);
        tempharm.clear();
    }
    return temptube;
} 

complex<float> structree::det(vector<complex<float> > &Y){
    complex<float> det(Y[0]*Y[3]-Y[1]*Y[2]);
    return det;
}

vector< vector< complex<float> > > structree::srs(vector<vector<complex<float> > > &templ,vector<vector<complex<float> > > &tempr){
    vector< vector< complex< float > > > temptube;
    for (int r=0;r<maxharmonic;r++){
        vector< complex< float > > tempharm;
        tempharm.push_back((det(templ[r])+templ[r][0]*tempr[r][0])/(tempr[r][0]+templ[r][3]));
        tempharm.push_back((-templ[r][1]*tempr[r][1])/(tempr[r][0]+templ[r][3]));
        tempharm.push_back((-templ[r][2]*tempr[r][2])/(tempr[r][0]+templ[r][3]));
        tempharm.push_back((det(tempr[r])+templ[r][3]*tempr[r][3])/(tempr[r][0]+templ[r][3]));
        temptube.push_back(tempharm);
        tempharm.clear();
    }
    return temptube;
}

vector <vector <complex<float> > > structree::admit(float r, int gen){
	bool can_loop=true;
	int repind;
	if (rmtrx[gen].size()>0){
		for (int p=0;p<rmtrx[gen].size();p++){
			if (rmtrx[gen][p]==r) {
				repind=p;
				can_loop=false;
			}
		}
	}
	if (can_loop==true){
		vector<vector<complex<float> > >adtube;
		vector<vector<complex<float> > >vadtube;
		rmtrx[gen].push_back(r);
		float vr;
		float L;
	        float vL;
		if (r>=0.005){
		    L=15.75*pow(r,1.10);
		}
		else{
		    L=1.79*pow(r,0.47);
		}
		vr=r*vr0/r0;
		vL=14.54*vr;
		complex<float>unitscale(10,0);
		complex<float>admat01(10*PI*pow(r,4)/(8*mu*L),0);
		complex<float>admat02(-10*PI*pow(r,4)/(8*mu*L),0);
		complex<float>admat03(-10*PI*pow(r,4)/(8*mu*L),0);
		complex<float>admat04(10*PI*pow(r,4)/(8*mu*L),0);
		admat.push_back(admat01);
		admat.push_back(admat02);
		admat.push_back(admat03);
		admat.push_back(admat04);
		adtube.push_back(admat);
		admat.clear();
		complex<float>admat09(10*PI*pow(vr,4)/(8*mu*vL),0);
		complex<float>admat10(-10*PI*pow(vr,4)/(8*mu*vL),0);
		complex<float>admat11(-10*PI*pow(vr,4)/(8*mu*vL),0);
		complex<float>admat12(10*PI*pow(vr,4)/(8*mu*vL),0);
		admat.push_back(admat09);
		admat.push_back(admat10);
		admat.push_back(admat11);
		admat.push_back(admat12);
		vadtube.push_back(admat);
		admat.clear();
		float w;
	        for (int freq=1;freq<maxharmonic;freq++){
	        	w=2*PI*freq/T;
			float wom=r*pow(w/mu,0.5);
			float vwom=vr*pow(w/mu,0.5);
			complex<float>K;
			complex<float>vK;
			if (wom>4){
			    complex<float>multarg(0,-PI/4);
			    K=1.0f-2.0f*exp(multarg)/w;
			}
			else{
			    complex<float>multarg(0,PI/2);
			    K=1.0f/(4.0f/3.0f-8.0f*exp(multarg)/(float)pow(w,2));
			}
			if (vwom>4){
			    complex<float>multarg(0,-PI/4);
			    vK=1.0f-2.0f*exp(multarg)/w;
			}
			else{
			    complex<float>multarg(0,PI/2);
			    vK=1.0f/(4.0f/3.0f-8.0f*exp(multarg)/(float)pow(w,2));
			}
			float comp(3*PI*pow(r,2)/(2*37.5));
			float vcomp(3*PI*pow(vr,2)/(2*37.5));
			complex<float> gw(sqrt(comp*(float)PI*(float)pow(r,2)*K/rho)); 
			complex<float> c(sqrt((float)PI*(float)pow(r,2)*K/(rho*comp)));
			complex<float> vgw(sqrt(vcomp*(float)PI*(float)pow(vr,2)*vK/rho)); 
			complex<float> vc(sqrt((float)PI*(float)pow(vr,2)*vK/(rho*vcomp)));
			complex<float> warg(0,PI/2);
			complex<float>admat05=-unitscale*exp(warg)*gw*cos(w*L/c)/(sin(w*L/c));
			complex<float>admat06=unitscale*exp(warg)*gw/(sin(w*L/c));
			complex<float>admat07=unitscale*exp(warg)*gw/(sin(w*L/c));
			complex<float>admat08=-unitscale*exp(warg)*gw*cos(w*L/c)/(sin(w*L/c));
			admat.push_back(admat05);
			admat.push_back(admat06);
			admat.push_back(admat07);
			admat.push_back(admat08);
			adtube.push_back(admat);
			admat.clear();
			complex<float>admat13=-unitscale*exp(warg)*vgw*cos(w*vL/vc)/(sin(w*vL/vc));
			complex<float>admat14=unitscale*exp(warg)*vgw/(sin(w*vL/vc));
			complex<float>admat15=unitscale*exp(warg)*vgw/(sin(w*vL/vc));
			complex<float>admat16=-unitscale*exp(warg)*vgw*cos(w*vL/vc)/(sin(w*vL/vc));
			admat.push_back(admat13);
			admat.push_back(admat14);
			admat.push_back(admat15);
			admat.push_back(admat16);
			vadtube.push_back(admat);
			admat.clear();
		}
		if (r*alpha>=rmin){
			gen=gen+1;
			vector <vector <complex<float> > > mid;
			if (r*beta>=rmin){
				vector <vector <complex<float> > > left=admit(r*alpha,gen);
				Ymtrx[gen].push_back(left);
				vector <vector <complex<float> > > right=admit(r*beta,gen);
				Ymtrx[gen].push_back(right);
				mid=prl(left,right);
			}
			else{
				mid=admit(r*alpha,gen);
				Ymtrx[gen].push_back(mid);
			}
			vector <vector <complex<float> > > efftp=srs(adtube,mid);
			vector <vector <complex<float> > > eff=srs(efftp,vadtube);
			if (gen==1){
				Ymtrx[gen-1].push_back(eff);
			}
			return eff;
		}
		else{
			vector <vector <complex<float> > > eff=srs(adtube,vadtube);
			Ymtrx[gen].push_back(eff);
			return eff;
		}
	}
	else{
		vector <vector <complex<float> > > eff=Ymtrx[gen][repind];
		return eff;
	}
}

vector<vector<float> > structree::initstruc(){

	cout<<"\nEvaluating structural tree impedance values begins....\n";
	int number=1;
	
	vector<float> yt1;
	vector<float> yt2;
	vector<float> yt3;
	vector<float> yt4;

	int nogen=floor(log(rmin/r0)/log(alpha))+1;
	rmtrx.resize(nogen);
	Ymtrx.resize(nogen);
	admit(r0,0);
	
	for (int i=0;i<maxharmonic;i++){
		Y1.push_back(Ymtrx[0][0][i][0]);
		Y2.push_back(Ymtrx[0][0][i][1]);
		Y3.push_back(Ymtrx[0][0][i][2]);
		Y4.push_back(Ymtrx[0][0][i][3]);
	}
	for (int i=0;i<maxharmonic;i++){
		Y1.push_back(Ymtrx[0][0][i][0]);
		Y2.push_back(Ymtrx[0][0][i][1]);
		Y3.push_back(Ymtrx[0][0][i][2]);
		Y4.push_back(Ymtrx[0][0][i][3]);
	}
	cout<<"yt1\t";
	yt1=ifft(Y1);
	cout<<yt1.size()<<"\n";
	cout<<"yt2\n";
	yt2=ifft(Y2);
	cout<<"yt3\n";
	yt3=ifft(Y3);
	cout<<"yt4\n";
	yt4=ifft(Y4);

	timeadmit.push_back(yt1);
	timeadmit.push_back(yt2);
	timeadmit.push_back(yt3);
	timeadmit.push_back(yt4);
	
	writeHist("examples/y1.csv",Y1);
	writeHist("examples/y2.csv",Y2);
	writeHist("examples/y3.csv",Y3);
	writeHist("examples/y4.csv",Y4);
	cout<<"\n....evaluating structural tree impedance values ends!\n";
	
	return timeadmit;
	
}
void structree::writeHist(const char* desti_File, vector<complex< float> > data)const {

        FILE *writfile;
        writfile=fopen(desti_File,"w");
        int m=data.size();
        for( int j=0; j<m; j++){
		fprintf(writfile, "%.14e,%.14e\n", real(data[j]),imag(data[j]));
        }
        fclose (writfile);
}
